package ru.aushakov.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.aushakov.tm.api.ServiceLocator;
import ru.aushakov.tm.api.repository.ICommandRepository;
import ru.aushakov.tm.api.service.ICommandService;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.exception.empty.EmptyArgumentException;
import ru.aushakov.tm.exception.empty.EmptyCommandException;
import ru.aushakov.tm.exception.empty.EmptyNameException;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class CommandService implements ICommandService {

    @NonNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        if (StringUtils.isEmpty(command.getName())) throw new EmptyNameException(command);
        commandRepository.add(command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@Nullable final String command) {
        if (StringUtils.isEmpty(command)) throw new EmptyCommandException();
        return commandRepository.getCommandByName(command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (StringUtils.isEmpty(arg)) throw new EmptyArgumentException();
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    @SneakyThrows
    public void initCommands(@NotNull final ServiceLocator serviceLocator) {
        @NotNull final Reflections reflections = new Reflections("ru.aushakov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        @NotNull List<Class<? extends AbstractCommand>> sortedClasses =
                classes.stream()
                        .sorted((Comparator.comparing(Class::getName)))
                        .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : sortedClasses) {
            int mods = clazz.getModifiers();
            if (Modifier.isAbstract(mods) || Modifier.isInterface(mods)) continue;
            @NotNull AbstractCommand command = clazz.newInstance();
            command.setServiceLocator(serviceLocator);
            add(command);
        }
    }

}
