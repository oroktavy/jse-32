package ru.aushakov.tm.api.service;

import ru.aushakov.tm.enumerated.ConfigProperty;

public interface IPropertyService {

    String getProperty(ConfigProperty property);

    Integer getIntProperty(ConfigProperty property);

}
