package ru.aushakov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum ConfigProperty {

    PASSWORD_SECRET("password.secret", ""),
    PASSWORD_ITERATION("password.iteration", "1"),
    DATAFILE_BINARY("datafile.binary", "./data.bin"),
    DATAFILE_BASE64("datafile.base64", "./data.base64"),
    DATAFILE_JSON("datafile.json", "./data.json"),
    DATAFILE_XML("datafile.xml", "./data.xml"),
    DATAFILE_JAXB_XML("datafile.jaxbxml", "./data-jaxb.xml"),
    DATAFILE_BACKUP("datafile.backup", "./backup.xml"),
    BACKUP_SLEEP_TIME("backup.sleeptime", "30"),
    FILE_SCANNER_SLEEP_TIME("filescanner.sleeptime", "10"),
    FILE_SCANNER_DIR("filescanner.dir", "./");

    @NotNull
    private final String propertyName;

    @NotNull
    private final String defaultValue;

    @NotNull
    private static final ConfigProperty[] staticValues = values();

    ConfigProperty(@NotNull final String propertyName, @NotNull final String defaultValue) {
        this.propertyName = propertyName;
        this.defaultValue = defaultValue;
    }

    @NotNull
    public String getPropertyName() {
        return propertyName;
    }

    @NotNull
    public String getDefaultValue() {
        return defaultValue;
    }

    @Nullable
    public static ConfigProperty toConfigProperty(@Nullable final String propertyId) {
        for (@NotNull final ConfigProperty property : staticValues) {
            if (property.name().equalsIgnoreCase(propertyId)) return property;
        }
        return null;
    }

}
