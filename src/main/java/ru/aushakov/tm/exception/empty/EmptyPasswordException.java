package ru.aushakov.tm.exception.empty;

public class EmptyPasswordException extends RuntimeException {

    public EmptyPasswordException() {
        super("Password can not be empty!");
    }

}
